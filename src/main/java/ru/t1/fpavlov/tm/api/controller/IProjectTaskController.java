package ru.t1.fpavlov.tm.api.controller;

/**
 * Created by fpavlov on 26.11.2021.
 */
public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskToProject();

}
